module.exports = {
  'env': {
    es2021: true,
    node: true,
  },
  'extends': [
    'eslint:recommended',
    'google',
  ],
  'parserOptions': {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  'rules': {
    'indent': [
      'error',
      2,
    ],
    'object-property-newline': ['error'],
    'object-curly-newline': ['error'],
    'require-jsdoc': 'off',
    'object-curly-spacing': [
      'error',
      'always',
    ],
    'array-bracket-newline': [
      'error',
      { 'minItems': 2 },
    ],
    'array-element-newline': [
      'error',
      'always',
    ],
  },
};
