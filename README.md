# perf-test

Performance testing for pyshare.

## How to run

Assume [pyShare](https://gitlab.com/pyshare/pyShare) is running locally. Change the env vars to fit your environment.

```shell
docker run -it --rm \
  -e PYSHARE_URL=http://web:8080 \
  -e ADMIN=madoka \
  -e ADMIN_PASS=madoka \
  --network pyshare_default \
  -v $PWD:/app -w /app \
  loadimpact/k6 run <script path>
```
