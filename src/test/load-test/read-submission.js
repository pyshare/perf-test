import PyShareAgent from '../../util/agent.js';
import arrange from '../../util/arrange.js';
import { randomString, randomItem } from 'https://jslib.k6.io/k6-utils/1.1.0/index.js';
import { sleep } from 'k6';
import { Rate, Trend } from 'k6/metrics';

export const options = {
  thresholds: {
    'http_req_duration{action:Read comment}': ['p(95)<1000'],
    'http_req_duration{action:Create comment}': ['p(95)<1000'],
    'http_req_duration{action:Read problem list}': ['p(95)<1000'],
    'http_req_duration{action:Read course}': ['p(95)<1000'],
  },
  stages: [
    {
      duration: '15s',
      target: 10,
    },
    {
      duration: '1m',
      target: 30,
    },
    {
      duration: '20s',
      target: 0,
    },
  ],
};


export function setup() {
  const course = arrange.course();
  const USER_CNT = 20;
  const PROBLEM_CNT = 3;
  const users = [];
  const problems = [];
  for (let i = 0; i < USER_CNT; i++) {
    const password = randomString(16);
    const user = arrange.user({ course: course.id,
      password });
    user.password = password;
    users.push(user);
    for (let j = 0; j < PROBLEM_CNT; j++) {
      problems.push(arrange.problem({
        course: course.id,
        author: user.id,
        isOJ: true,
      }));
    }
  }
  return {
    course,
    users,
    problems,
  };
}

const submissionSuccessful = new Rate('submission_successful');
const submissionWaiting = new Trend('submission_waiting');

export default function(data) {
  const agent = new PyShareAgent();
  // Login
  /* eslint-disable no-undef */
  const { username, password } = data.users[__VU % data.users.length];
  agent.post('/auth/session', JSON.stringify({
    username,
    password,
    school: '',
  }), {
    headers: {
      'Content-Type': 'application/json',
    },
  });
  // View course
  const courseRes = agent.get(
    `/course/${data.course.id}`,
    { tags: { action: 'Read course' } },
  );
  if (courseRes.status !== 200) {
    return;
  }
  sleep(Math.random() * 0.5 + 1);
  // View random problem
  const courseData = JSON.parse(courseRes.body).data;
  const problemRes = agent.get(
    `/problem?offset=0` +
    `&count=-1&course=${courseData.id}`,
    { tags: { action: 'Read problem list' } },
  );
  if (problemRes.status !== 200) {
    return;
  }
  const problemData = JSON.parse(problemRes.body).data;
  const { pid } = randomItem(problemData);
  const commentRes = agent.post('/comment', JSON.stringify({
    target: 'problem',
    id: pid,
    title: randomString(8),
    content: randomString(64),
    code: 'print("Nya~ hello~")',
  }), {
    headers: {
      'Content-Type': 'application/json',
    },
    tags: {
      action: 'Create Comment',
    },
  });
  if (commentRes.status !== 200) {
    return;
  }
  const commentData = JSON.parse(commentRes.body).data;
  for (let i = 0; i < 10; i++) {
    const res = agent.get(
      `/comment/${commentData.id}`,
      { tags: { action: 'Read comment' } },
    );
    if (res.status === 200) {
      const { submission } = JSON.parse(res.body).data;
      if (submission.stdout !== undefined) {
        submissionSuccessful.add(1);
        submissionWaiting.add(i);
        return;
      }
    }
    sleep(1);
  }
  submissionSuccessful.add(0);
}
