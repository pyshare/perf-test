import PyShareAgent from '../../util/agent.js';
import arrange from '../../util/arrange.js';
import { randomString, randomItem } from 'https://jslib.k6.io/k6-utils/1.1.0/index.js';
import { sleep } from 'k6';

export const options = {
  stages: [
    {
      duration: '5s',
      target: 10,
    },
    {
      duration: '15s',
      target: 30,
    },
    {
      duration: '30s',
      target: 30,
    },
    {
      duration: '10s',
      target: 0,
    },
  ],
};


export function setup() {
  const course = arrange.course();
  const USER_CNT = 20;
  const PROBLEM_CNT = 3;
  const users = [];
  const problems = [];
  for (let i = 0; i < USER_CNT; i++) {
    const password = randomString(16);
    const user = arrange.user({ course: course.id,
      password });
    user.password = password;
    users.push(user);
    for (let j = 0; j < PROBLEM_CNT; j++) {
      problems.push(arrange.problem({
        course: course.id,
        author: user.id,
        isOJ: true,
      }));
    }
  }
  return {
    course,
    users,
    problems,
  };
}

export default function(data) {
  const agent = new PyShareAgent();
  // Login
  /* eslint-disable no-undef */
  const { username, password } = data.users[__VU % data.users.length];
  agent.post('/auth/session', JSON.stringify({
    username,
    password,
    school: '',
  }), {
    headers: {
      'Content-Type': 'application/json',
    },
  });
  // View course
  const courseRes = agent.get(`/course/${data.course.id}`);
  if (courseRes.status !== 200) {
    return;
  }
  sleep(Math.random() * 0.5 + 1);
  // View random problem
  const courseData = JSON.parse(courseRes.body).data;
  const problemRes = agent.get(
    `/problem?offset=0` +
    `&count=-1&course=${courseData.id}`);
  if (problemRes.status !== 200) {
    return;
  }
  const problemData = JSON.parse(problemRes.body).data;
  for (let i = 0; i < 3; i++) {
    const { pid } = randomItem(problemData);
    agent.get(`/problem/${pid}`);
  }
}
