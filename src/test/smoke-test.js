import PyShareAgent from '../util/agent.js';

export default function() {
  const payload = JSON.stringify({
    username: 'madoka',
    password: 'madoka',
    school: '',
  });
  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const agent = new PyShareAgent();
  agent.post('/auth/session', payload, params);
  agent.get('/course');
}
