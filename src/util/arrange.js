import PyShareAgent from './agent.js';

/* eslint-disable no-undef */
const { ADMIN, ADMIN_PASS } = __ENV;

function authedAgent() {
  const agent = new PyShareAgent();
  agent.post('/auth/session', JSON.stringify({
    username: ADMIN,
    password: ADMIN_PASS,
    school: '',
  }), {
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return agent;
}

function createCourse() {
  const agent = authedAgent();
  const res = agent.post('/dummy/course', '{}', {
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return JSON.parse(res.body).data;
}
function createUser({
  course, password,
} = {}) {
  const agent = authedAgent();
  const res = agent.post('/dummy/user', JSON.stringify({
    course,
    password,
  }), {
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return JSON.parse(res.body).data;
}

function createProblem({
  course, author, isOJ,
} = {}) {
  const agent = authedAgent();
  const res = agent.post('/dummy/problem', JSON.stringify({
    course,
    author,
    isOJ,
    allowMultipleComments: true,
  }), {
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return JSON.parse(res.body).data;
}

export default {
  user: createUser,
  course: createCourse,
  problem: createProblem,
};
