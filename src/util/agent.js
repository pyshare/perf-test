import http from 'k6/http';

class PyShareAgent {
  constructor() {
    /* eslint-disable no-undef */
    const {
      PYSHARE_URL,
    } = __ENV;
    this.BASE_URL = PYSHARE_URL;
  }

  get(url, params) {
    return http.get(`${this.BASE_URL}${url}`, params);
  }

  post(url, payload, params) {
    return http.post(`${this.BASE_URL}${url}`, payload, params);
  }

  put(url, payload, params) {
    return http.put(`${this.BASE_URL}${url}`, payload, params);
  }

  delete(url, payload, params) {
    return http.del(`${this.BASE_URL}${url}`, payload, params);
  }
}

export default PyShareAgent;
